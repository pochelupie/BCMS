DROP TABLE  "BCMS_SESSION_FIRE_TRUCK";
DROP TABLE  "BCMS_SESSION_POLICE_VEHICLE";
DROP TABLE  "FIRE_TRUCK";
DROP TABLE  "POLICE_VEHICLE";
DROP TABLE  "ROUTE";
DROP TABLE  "EVENT";
DROP TABLE  "BCMS_SESSION";

--'session is instantiate when we use the singleton BCMS
create table BCMS_session(
session_id varchar(30),
constraint BCMS_session_key primary key(session_id)); 


create table Event(
event_name varchar(150),
execution_trace varchar(500),
session_id varchar(30),
constraint Event_key primary key(event_name),
constraint BCMS_session_foreign_key foreign key(session_id) references BCMS_session(session_id) on delete cascade);

create table Fire_truck(
fire_truck_name varchar(30),
constraint Fire_truck_key primary key(fire_truck_name));

create table Police_vehicle(
police_vehicle_name varchar(30),
constraint Police_vehicle_key primary key(police_vehicle_name));

create table Route(
route_name varchar(30),
constraint Route_key primary key(route_name));

create table BCMS_session_Fire_truck(
session_id varchar(30),
fire_truck_name varchar(30),
fire_truck_status varchar(10) CONSTRAINT fire_truck_status_check CHECK (fire_truck_status IN ('Idle','Dispatched','Arrived','Blocked','Breakdown')),
constraint BCMS_session_foreign_key2 foreign key(session_id) references BCMS_session(session_id) on delete cascade,
constraint Fire_truck_foreign_key foreign key(fire_truck_name) references Fire_truck(fire_truck_name) on delete cascade,
constraint BCMS_session_Fire_truck_key primary key(session_id,fire_truck_name));
--' In JEE layer we can compute number of vehicle for the session

create table BCMS_session_Police_vehicle(
session_id varchar(30),
police_vehicle_name varchar(30),
police_vehicle_status varchar(10) CONSTRAINT police_vehicle_status_check CHECK (police_vehicle_status IN ('Idle','Dispatched','Arrived','Blocked','Breakdown')),
constraint BCMS_session_foreign_key3 foreign key(session_id) references BCMS_session(session_id) on delete cascade,
constraint Police_vehicle_foreign_key foreign key(police_vehicle_name) references Police_vehicle(police_vehicle_name) on delete cascade,
constraint BCMS_session_Police_vehicle_key primary key(session_id,police_vehicle_name));

insert into FIRE_TRUCK values ('FT1');
insert into FIRE_TRUCK values ('FT2');
insert into FIRE_TRUCK values ('FT3');

insert into POLICE_VEHICLE values ('PV1');
insert into POLICE_VEHICLE values ('PV2');
insert into POLICE_VEHICLE values ('PV3');

insert into ROUTE values ('route for fire trucks');
insert into ROUTE values ('route for police vehicles');
insert into ROUTE values ('r1');
insert into ROUTE values ('r2');
insert into ROUTE values ('r3');
insert into ROUTE values ('r4');
insert into ROUTE values ('r5');
