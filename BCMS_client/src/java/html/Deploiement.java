/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package html;

import com.pauware.pauware_engine._Exception.Statechart_exception;
import entity_bean.BcmsSession;
import entity_bean.BcmsSessionFireTruck;
import entity_bean.BcmsSessionPoliceVehicle;
import entity_bean.FireTruck;
import entity_bean.PoliceVehicle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UISelectItem;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import service.Service;

@ManagedBean(name = "Deploiement")
@RequestScoped
public class Deploiement {

    private List<BcmsSessionFireTruck> fire_trucks;
    private List<BcmsSessionPoliceVehicle> police_vehicles;

    private HtmlDataTable psc_htmlDataTable;
    private HtmlDataTable fsc_htmlDataTable;

    ////////// GETTER AND SETTER
    public HtmlDataTable getPsc_htmlDataTable() {
        return psc_htmlDataTable;
    }

    public void setPsc_htmlDataTable(HtmlDataTable psc_htmlDataTable) {
        this.psc_htmlDataTable = psc_htmlDataTable;
    }

    public HtmlDataTable getFsc_htmlDataTable() {
        return fsc_htmlDataTable;
    }

    public void setFsc_htmlDataTable(HtmlDataTable fsc_htmlDataTable) {
        this.fsc_htmlDataTable = fsc_htmlDataTable;
    }

    public List<BcmsSessionFireTruck> getFire_trucks() {
        return fire_trucks;
    }

    public void setFire_trucks(ArrayList<BcmsSessionFireTruck> fire_trucks) {
        this.fire_trucks = fire_trucks;
    }

    public List<BcmsSessionPoliceVehicle> getPolice_vehicles() {
        return police_vehicles;
    }

    public void setPolice_vehicles(ArrayList<BcmsSessionPoliceVehicle> police_vehicles) {
        this.police_vehicles = police_vehicles;
    }

    ///////// load data
    @PostConstruct
    public void load() {
        load_fire_trucks();
        load_police_vehicles();
    }

    private void load_fire_trucks() {
        //get fire_trucks
        fire_trucks = Service.getInstance().getFsc().get_list_fire_truck_by_session();
    }

    private void load_police_vehicles() {
        police_vehicles = Service.getInstance().getPsc().get_list_police_vehicle_by_session();
    }

    ///////////////// list of vehicule to replace. CURRENTLY NOT USED
    public ArrayList<String> psc_replaceBy(String pv) {
        ArrayList<String> new_possible = new ArrayList();

        for (BcmsSessionPoliceVehicle pvn : police_vehicles) {
            new_possible.add(pv);
        }

        return new_possible;
    }

    ////////////// call to the next page
    public void psc_submit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("summary_psc.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fsc_submit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("summary_fsc.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    ////////// other police vehicule to replace this one
    public ArrayList<String> other_police_vehicles(String current_pv) {
        ArrayList<String> others = new ArrayList<>();

        others.add("aucun");

        for (BcmsSessionPoliceVehicle pv : police_vehicles) {
            if (!pv.getPoliceVehicle().getPoliceVehicleName().equals(current_pv) &&
                    pv.getPoliceVehicleStatus().equals("Idle")) {
                others.add(pv.getPoliceVehicle().getPoliceVehicleName());
            }
        }

        return others;
    }

    public ArrayList<String> other_fire_trucks(String current_ft) {
        ArrayList<String> others = new ArrayList<>();

        others.add("aucun");

        for (BcmsSessionFireTruck ft : fire_trucks) {
            if (!ft.getFireTruck().getFireTruckName().equals(current_ft) &&
                    ft.getFireTruckStatus().equals("Idle")) {
                others.add(ft.getFireTruck().getFireTruckName());
            }
        }

        return others;
    }

    //////////call to replace a vehicule breakdown
    public void psc_breakdown(ValueChangeEvent vce) {

        String vehicle_breakdown = (String) vce.getComponent().getAttributes().get("vehicle_breakdown");
        String replace = (String) vce.getNewValue();
        if (replace.equals("aucun")) {
            replace = null;
        }
        try {
            Service.getInstance().getPsc().police_vehicle_breakdown(vehicle_breakdown, replace);
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fsc_breakdown(ValueChangeEvent vce) {
        String vehicle_breakdown = (String) vce.getComponent().getAttributes().get("vehicle_breakdown");
        String replace = (String) vce.getNewValue();
        if (replace.equals("aucun")) {
            replace = null;
        }
        try {
            Service.getInstance().getFsc().fire_truck_breakdown(vehicle_breakdown, replace);
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /////////call when change status

    public void psc_update(BcmsSessionPoliceVehicle pv) {
        String id = pv.getPoliceVehicle().getPoliceVehicleName();
        try {

            switch (pv.getPoliceVehicleStatus()) {
                case "Arrived":
                    Service.getInstance().getPsc().police_vehicle_arrived(id);
                    break;
                case "Dispatched":
                    Service.getInstance().getPsc().police_vehicle_dispatched(id);
                    break;
                case "Breakdown":
                    Service.getInstance().getPsc().police_vehicle_breakdown(id, null);
                    break;
                case "Blocked":
                    Service.getInstance().getPsc().police_vehicle_blocked(id);
                    break;

            }
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //load_fire_trucks();//useful??
    }

    public void fsc_update(BcmsSessionFireTruck ft) {
        String id = ft.getFireTruck().getFireTruckName();
        try {

            switch (ft.getFireTruckStatus()) {
                case "Arrived":
                    Service.getInstance().getFsc().fire_truck_arrived(id);
                    break;
                case "Dispatched":
                    Service.getInstance().getFsc().fire_truck_dispatched(id);
                    break;
                case "Breakdown":
                    Service.getInstance().getFsc().fire_truck_breakdown(id, null);
                    break;
                case "Blocked":
                    Service.getInstance().getFsc().fire_truck_blocked(id);
                    break;

            }
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    ///////// route management by police
    public void no_more_route_left() {

        try {
            Service.getInstance().getPsc().no_more_route_left();
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void crisis_is_less_severe() {
        try {
            Service.getInstance().getPsc().crisis_is_less_severe();
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void crisis_is_more_severe() {
        try {
            Service.getInstance().getPsc().crisis_is_more_severe();
        } catch (Statechart_exception ex) {
            Logger.getLogger(Deploiement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
