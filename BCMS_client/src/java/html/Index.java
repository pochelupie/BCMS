package html;

import com.pauware.pauware_engine._Exception.Statechart_exception;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.Instance;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import service.Service;
import services.FSC;
import services.PSC;

@ManagedBean(name = "Index")
@RequestScoped
public class Index {

    // Default constructor
    public Index() {
    }

    //Test BCMS process
    public String test() {
        StringBuilder log = new StringBuilder("");
        try {

            javax.naming.Context jndi_context = new javax.naming.InitialContext();

            log.append("1> LA PHASE DE LANCEMENT DU SERVEUR COMMENCE !!!! <br/>");
            log.append("=> On lance le serveur ... <br/>");

            FSC fsc = (FSC) jndi_context.lookup("java:global/BCMS_client/BCMS!services.FSC");
            PSC psc = (PSC) jndi_context.lookup("java:global/BCMS_client/BCMS!services.PSC");

            log.append("2> LA PHASE DE CONNECTION COMMENCE !!!! Tous le monde doit etre connecte pour passer a l'etape 3<br/>");
            log.append("=> Les pompiers se connectent ...<br/>");
            fsc.FSC_connection_request();

            log.append("=> Les policiers se connectent ...<br/>");
            psc.PSC_connection_request();

            log.append("=> Les pompiers declarent avoir 2 vehicules <br/>");
            fsc.state_fire_truck_number(2); // firemen have 2 trucks. NOUS : instancier 2

            log.append("=> Les policiers declarent avoir 2 vehicules <br/>");
            psc.state_police_vehicle_number(2); //policemen have 2 vehicles.
                    
            log.append("3> LA PHASE DE NEGOTIATION DE ROUTE COMMENCE !!!! <br/>");

            // Phase de negotiation pour se mettre d'accord sur la route à utiliser. Pas de persistance dans la BDD
            log.append("=> Les policiers demande la permission d'utiliser une route aux pompiers<br/>");
            try {
                psc.route_for_police_vehicles("route for police vehicles");
                Thread.sleep(100);
            } catch (Statechart_exception e) {
                log.append("<FONT COLOR='red'>").append(e.getMessage()).append("</FONT> <br/>");
            }
            /**
             * Bug due to PlantUML modeling style. One must await so that 'route_for_police_vehicles' that is re-sent internally, arrives before what follows:
             */
            log.append("=> Les policiers proposent une route aux pompiers <br/>");
            try {
                psc.route_for_fire_trucks("route for fire trucks");
            } catch (Statechart_exception e) {
                log.append("<FONT COLOR='red'>").append(e.getMessage()).append("</FONT> <br/>");
            }

            log.append("=> Les pompiers ne sont pas d'accord avec cette route <br/>");
            fsc.FSC_disagrees_about_fire_truck_route();

            log.append("=> Les policiers proposent une (nouvelle) route aux pompiers <br/>");
            try {
                psc.route_for_fire_trucks("route_name");
            } catch (Statechart_exception e) {
                log.append("<FONT COLOR='red'>").append(e.getMessage()).append("</FONT> <br/>");
            }

            // On enregistre la route dans la bdd :
            log.append("=> Les pompiers sont d'accord avec la route pour les policiers <br/>");
            fsc.FSC_agrees_about_police_vehicle_route();

            log.append("=> Les pompiers sont d'accord avec la route qu'ils doivent prendre <br/>");
            fsc.FSC_agrees_about_fire_truck_route();
            Thread.sleep(100);
            /**
             * Bug due to PlantUML modeling style. One must await so that 'completion' that is sent internally, arrives before what follows:
             */

            log.append("4> LA PHASE DE DEPLOIEMENT DES VEHICULES COMMENCE !!!! <br/>");

            //Policier et Pompier envoie les vehicules : modifier l'etat. Par défaut c'est Idle
            log.append("=> Les pompiers envoit FT1 <br/>");
            fsc.fire_truck_dispatched("FT1");
            Thread.sleep(100);

            log.append("=> Les pompiers envoit FT2 <br/>");
            fsc.fire_truck_dispatched("FT2");
            Thread.sleep(100);

            log.append("=> Les policiers envoit PV1 <br/>");
            psc.police_vehicle_dispatched("PV1");
            Thread.sleep(100);

            log.append("=> Les policiers envoit PV2 <br/>");
            psc.police_vehicle_dispatched("PV2");
            Thread.sleep(100);

            log.append("=> Les policiers declare que PV1 est tombé en panne <br/>");
            psc.police_vehicle_breakdown("PV1", null);

            log.append("=> Les pompiers déclarent que FT1 est arrivé <br/>");
            fsc.fire_truck_arrived("FT1");

            log.append("=> Les pompiers déclarent que FT2 est arrivé <br/>");
            fsc.fire_truck_arrived("FT2");

            log.append("=> Les policiers declare que PV2 est arrivé <br/>");
            psc.police_vehicle_arrived("PV2");
            



            //'PV1' had an accident above, so between comments:
//            psc.police_vehicle_arrived("PV1");
            Thread.sleep(100);
//
//                log.append("5> LA PHASE DE DECONNEXION COMMENCE !!!! : On pourrait demander a l'utilisateur si il souhaite quitter ou relancer une nouvelle crise<br/>");
//                bCMS.close();

//            ///////////////////////////////RESET/////////////////////////////////////
//                Thread.sleep(100);
//                bCMS.reset();
//
//                log.append("!!!!!!!!!!RESET!!!!!!!!!!!!<br/>");
//
//                Thread.sleep(100);
//                bCMS.FSC_connection_request();
//                bCMS.PSC_connection_request();
//                bCMS.state_fire_truck_number(4);
//                bCMS.state_police_vehicle_number(2);
//
//                bCMS.route_for_police_vehicles("route_name<br/>");
//                Thread.sleep(100);
//                /**
//                 * Bug due to PlantUML modeling style. One must await so that 'route_for_police_vehicles' that is re-sent internally, arrives before what follows:
//                 */
//                bCMS.route_for_fire_trucks("route_name<br/>");
//                bCMS.FSC_agrees_about_police_vehicle_route();
//                bCMS.FSC_agrees_about_fire_truck_route();
//                Thread.sleep(100);
//                /**
//                 * Bug due to PlantUML modeling style. One must await so that 'completion' that is sent internally, arrives before what follows:
//                 */
//
//                bCMS.fire_truck_dispatched("FT1<br/>");
//                Thread.sleep(100);
//                bCMS.fire_truck_dispatched("FT2<br/>");
//                Thread.sleep(100);
//                bCMS.fire_truck_dispatched("FT3<br/>");
//                Thread.sleep(100);
//                bCMS.fire_truck_dispatched("FT4<br/>");
//                Thread.sleep(100);
//                bCMS.police_vehicle_dispatched("PV1<br/>");
//                Thread.sleep(100);
//                bCMS.police_vehicle_dispatched("PV2<br/>");
//                Thread.sleep(100);
//                bCMS.fire_truck_breakdown("FT1", "FT1_outsider<br/>");
//                bCMS.fire_truck_arrived("FT4<br/>");
//                bCMS.fire_truck_arrived("FT2<br/>");
//                bCMS.fire_truck_arrived("FT3<br/>");
//                bCMS.police_vehicle_arrived("PV1<br/>");
//                bCMS.police_vehicle_arrived("PV2<br/>");
//                bCMS.crisis_is_less_severe();
//                Thread.sleep(100);
//                bCMS.close();
//                /**
//                 * End of state machine is delayed so that events sent internally are effectively processed:
//                 */
//                Thread.sleep(100);
//                bCMS.stop();
//
//                log.append("=>fin<br/>");
            if (jndi_context != null) {
                jndi_context.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }

        return log.toString();
    }

    public void submit(String user_name) {
        //take values of previous page
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) ctx.getExternalContext().getRequest();
        int nbVehicles = Integer.parseInt(request.getParameter("nbVehicles"));
        
        //call BCMS service
        if (user_name.equals("psc")) {
            try {
                Service.getInstance().getPsc().PSC_connection_request();
                Service.getInstance().getPsc().state_police_vehicle_number(nbVehicles);//write FT1, FT2, FT3 etc...
            } catch (Statechart_exception ex) {
                Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Service.getInstance().getFsc().FSC_connection_request();
                Service.getInstance().getFsc().state_fire_truck_number(nbVehicles);
            } catch (Statechart_exception ex) {
                Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // new page
        try {
            if (user_name.equals("psc")) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("negotiation_psc.xhtml");
            } else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("negotiation_fsc.xhtml");
            }
        } catch (IOException ex) {
            Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
