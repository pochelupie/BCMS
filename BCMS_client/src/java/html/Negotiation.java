package html;

import com.pauware.pauware_engine._Exception.Statechart_exception;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.servlet.http.HttpServletRequest;
import service.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author eisti
 */
@ManagedBean(name = "Negotiation")
@ApplicationScoped //Singleton
public class Negotiation {

    final String waiting_psc = "En attente des policiers ...";
    final String waiting_fsc = "En attente des pompiers ...";
    final String OK = "OK";
    final String KO = "KO";

    //Update by policemen
    String route_for_PV = waiting_psc;//route for police vehicles
    String route_for_FT = waiting_psc;//route for fire trucks

    //Updated by firemen
    String route_for_PV_comment = waiting_fsc; //"en attente" , "OK" , "KO"
    String route_for_FT_comment = waiting_fsc;

    public String getRoute_for_PV() {
        return route_for_PV;
    }

    public void setRoute_for_PV(String route_for_PV) {
        this.route_for_PV = route_for_PV;
    }

    public String getRoute_for_FT() {
        return route_for_FT;
    }

    public void setRoute_for_FT(String route_for_FT) {
        this.route_for_FT = route_for_FT;
    }

    public String getRoute_for_PV_comment() {
        return route_for_PV_comment;
    }

    public void setRoute_for_PV_comment(String route_for_PV_OK) {
        this.route_for_PV_comment = route_for_PV_OK;
    }

    public String getRoute_for_FT_comment() {
        return route_for_FT_comment;
    }

    public void setRoute_for_FT_comment(String route_for_FT_OK) {
        this.route_for_FT_comment = route_for_FT_OK;
    }

    //CALL BY FIREMEN
    public void about_fire_truck_route(String comment) {
        if (route_for_police_vehicles_exist(route_for_FT)) {
            if (!route_for_FT_comment.equals(waiting_psc)) {
                if (comment.equals("OK")) {
                    try {
                        route_for_FT_comment = OK;
                        Service.getInstance().getFsc().FSC_agrees_about_fire_truck_route();
                    } catch (Statechart_exception ex) {
                        Logger.getLogger(Negotiation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        route_for_FT_comment = KO;
                        Service.getInstance().getFsc().FSC_disagrees_about_fire_truck_route();
                    } catch (Statechart_exception ex) {
                        Logger.getLogger(Negotiation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void about_police_vehicle_route(String comment) {
        if (route_for_police_vehicles_exist(route_for_PV)) {
            if (!route_for_FT_comment.equals(waiting_psc)) {
                if (comment.equals("OK")) {
                    try {
                        route_for_PV_comment = OK;
                        Service.getInstance().getFsc().FSC_agrees_about_police_vehicle_route();
                    } catch (Statechart_exception ex) {
                        Logger.getLogger(Negotiation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        route_for_PV_comment = KO;
                        Service.getInstance().getFsc().FSC_disagrees_about_police_vehicle_route();
                    } catch (Statechart_exception ex) {
                        Logger.getLogger(Negotiation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void fsc_submit() {
        if (route_for_FT_comment.equals(OK) && route_for_PV_comment.equals(OK)) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("deploiement_fsc.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //CALL BY POLICEMEN
    public void route_for_police_vehicles(String route) {
        if (route_for_police_vehicles_exist(route)) {
            //update view
            route_for_PV = route;
            route_for_PV_comment = waiting_fsc;
        } else {
            route_for_PV = waiting_psc;
            route_for_PV_comment = waiting_fsc;
        }
    }

    public void route_for_fire_trucks(String route) {
        if (route_for_police_vehicles_exist(route)) {
            //update view
            route_for_FT = route;
            route_for_FT_comment = waiting_fsc;
        } else {
            route_for_FT = waiting_psc;
            route_for_FT_comment = waiting_fsc;
        }
    }

    //display exception if false
    public boolean route_for_police_vehicles_exist(String route) {
        boolean res;
        try {
            Service.getInstance().getPsc().route_for_police_vehicles(route);
            res = true;
        } catch (Statechart_exception ex) {
            FacesContext.getCurrentInstance().addMessage("route_for_police_vehicles", new FacesMessage("Error: " + ex));
            Logger.getLogger(Negotiation.class.getName()).log(Level.SEVERE, null, ex);
            res = false;
        }
        return res;
    }

    public boolean route_for_fire_trucks_exist(String route) {
        boolean res;
        try {
            Service.getInstance().getPsc().route_for_fire_trucks(route);
            res = true;
        } catch (Statechart_exception ex) {
            FacesContext.getCurrentInstance().addMessage("route_for_fire_trucks", new FacesMessage("Error: " + ex));
            Logger.getLogger(Negotiation.class.getName()).log(Level.SEVERE, null, ex);
            res = false;
        }
        return res;
    }

    public void psc_submit() {
        if (route_for_FT_comment.equals(OK) && route_for_PV_comment.equals(OK)) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("deploiement_psc.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // recupere les routes et les affichent sous forme de listes
    public String list_Of_route() throws Statechart_exception {
        String servicesCalled = Service.getInstance().getPsc().getAllRoute();
        String s = servicesCalled.replaceAll(";", "<li/>");
        s = "<ul><li/>" + s + "</ul>";
        return s;
    }

}
