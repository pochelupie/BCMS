/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package html;

import entity_bean.BcmsSession;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import service.Service;

/**
 *
 * @author eisti
 */
@ManagedBean(name = "Summary")
@RequestScoped
public class Summary {
   public String psc(){
       BcmsSession session=Service.getInstance().getPsc().PSC_BCMSsession();
       String servicesCalled = Service.getInstance().getPsc().getEventPSC();
       String s=servicesCalled.replaceAll(";","<li/>");
       s = "<ul><li/>"+s+"</ul>";
       return s;
   }
   
   public String fsc(){
       BcmsSession session=Service.getInstance().getFsc().FSC_BCMSsession();
       String servicesCalled = Service.getInstance().getFsc().getEventFSC();
       String s=servicesCalled.replaceAll(";","<li/>");
       s = "<ul><li/>"+s+"</ul>";
       return s;
   }
}
