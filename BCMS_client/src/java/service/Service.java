package service;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Startup;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Singleton;
import javax.naming.NamingException;
import services.FSC;
import services.PSC;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author eisti
 */
@ManagedBean
@ApplicationScoped
public class Service implements Serializable {

    PSC psc = null;
    FSC fsc = null;

    static Service instance=null;
    
    public static Service getInstance() {
        if (instance==null){
            instance = new Service();
            
        }
        
        return instance;
    }

    private Service() {
        try {
            javax.naming.Context jndi_context = new javax.naming.InitialContext();
            psc = (PSC) jndi_context.lookup("java:global/BCMS_client/BCMS!services.PSC");
            fsc = (FSC) jndi_context.lookup("java:global/BCMS_client/BCMS!services.FSC");
        } catch (NamingException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



    @PreDestroy
    private void destroy() {
        psc = null;
        fsc = null;
    }

    public PSC getPsc() {
        return psc;
    }

    public FSC getFsc() {
        return fsc;
    }

    public void setPsc(PSC psc) {
        this.psc = psc;
    }

    public void setFsc(FSC fsc) {
        this.fsc = fsc;
    }

}
