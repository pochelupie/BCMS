package data_manager;

import entity_bean.BcmsSession;
import entity_bean.Event;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.EntityManager;

/**
 *
 * @author eisti This class is used to manage event table
 */
public class Event_manager {

    @javax.persistence.PersistenceContext(name = "BCMS_serverPU")
    private static EntityManager _entity_manager;

    private static boolean display = true;

    public static void init(EntityManager entity_manager) {

        _entity_manager = entity_manager;
    }

    /**
     * create a event in the database
     *
     * @param session
     * @param request
     * @param exec_trace
     */
    public static void createEvent(BcmsSession session, String request, String exec_trace) {
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
            Event event = new Event();
            String name = request + " - " + sdf.format(cal.getTime()) + System.nanoTime();
            event.setEventName(name);
            event.setSessionId(session);
            event.setExecutionTrace(exec_trace);

            if (display) {
                System.out.println("EVENT : " + name);
            }
            _entity_manager.persist(event);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * Show all event by session id
     *
     * @param session
     * @return
     */
    public static java.util.List<Event> get_Event_bySession(BcmsSession session) {
        assert (_entity_manager != null);
        return _entity_manager.createNamedQuery("Event.findBySessionId").setParameter("sessionId", session).getResultList();
    }


    public static void error(String output, Exception e) {
        System.err.println("ERROR : " + output + " " + e.getMessage());
    }
}
