package data_manager;

import entity_bean.Route;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class Route_manager {

    @javax.persistence.PersistenceContext(name = "BCMS_serverPU")
    private static EntityManager _entity_manager;

    public static void init(EntityManager entity_manager){
        _entity_manager=entity_manager;
    }
    
    public static void in_Route_for_fire_trucks_approved(){}
    public static void in_Route_for_police_vehicles_approved(){}
    public static void no_more_route_left(){}
    
    public static String get_route_for_police_vehicles(String route_name){
        String res=null;
        try{
            Route _last_police_vehicle_route = _entity_manager.find(Route.class, route_name); // On construit un entity bean 'Route' avec sa clef 'route_name' ; on le cherche dans la base...
            res=_last_police_vehicle_route.getRouteName();
        }catch(Exception e){
            System.err.println(e);
        }
        
        return res;
    }
    
    public static String get_route_for_fire_trucks(String route_name){
        String res=null;    
        try{
            Route _last_fire_truck_route = _entity_manager.find(Route.class, route_name); // On construit un entity bean 'Route' avec sa clef 'route_name' ; on le cherche dans la base...
            res=_last_fire_truck_route.getRouteName();
        }catch(Exception e){
            System.err.println(e);
        }
        
        return res;
    }
    
    public static void insert_route(String routeName){
        try
        {
            Route route = new Route(routeName);
            _entity_manager.persist(route);
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
    }
    
    public static java.util.List<Route> all_route(){
        assert (_entity_manager != null);
        return _entity_manager.createNamedQuery("Route.findAll").getResultList();
    }
}
