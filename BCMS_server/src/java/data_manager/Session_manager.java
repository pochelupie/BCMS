package data_manager;

import entity_bean.BcmsSession;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class Session_manager {

    @javax.persistence.PersistenceContext(name = "BCMS_serverPU")
    private static EntityManager _entity_manager;

    public static void init(EntityManager entity_manager){
        _entity_manager=entity_manager;
    }
    
    /**
     * Insert session ID in the data base when the session start.
     * 
     */
    public static BcmsSession insert_session(String session)
    {
        try
        {
            BcmsSession _session_Start = new BcmsSession(session);
            _entity_manager.persist(_session_Start);
            return _session_Start;
        }
        catch(Exception e)
        {
            System.err.println(e);
            return null;
        }
    }
    

}
