package data_manager;

import entity_bean.BcmsSession;
import entity_bean.BcmsSessionFireTruck;
import entity_bean.BcmsSessionFireTruckPK;
import entity_bean.BcmsSessionPoliceVehicle;
import entity_bean.BcmsSessionPoliceVehiclePK;
import entity_bean.FireTruck;
import entity_bean.PoliceVehicle;
import java.util.Collections;
import java.util.Comparator;
import javax.persistence.EntityManager;

public class Vehicle_manager {

    @javax.persistence.PersistenceContext(name = "BCMS_serverPU")
    private static EntityManager _entity_manager;

    public static void init(EntityManager entity_manager) {
        _entity_manager = entity_manager;
    }

    public static void flushAndClear() {
        _entity_manager.flush();
        _entity_manager.clear();
    }

    // ***** POLICE VEHICLE ***** //\
    /**
     * Insert police vehicle session in the data base. insert the name of the vehicle, his status and the active session
     */
    public static void insert_session_police_vehicle(BcmsSession session, PoliceVehicle police_vehicle, String status) {
        BcmsSessionPoliceVehicle _police_vehicule = new BcmsSessionPoliceVehicle(session.getSessionId(), police_vehicle.getPoliceVehicleName());
        _police_vehicule.setPoliceVehicleStatus(status);

        _police_vehicule.setPoliceVehicleStatus(status);
        _police_vehicule.setBcmsSession(session);
        _police_vehicule.setPoliceVehicle(police_vehicle);
        _entity_manager.persist(_police_vehicule);
    }

    public static void update_session_police_vehicle(BcmsSession session, String police_vehicle, String status) {
        BcmsSessionPoliceVehicle _police_vehicule = _entity_manager.find(BcmsSessionPoliceVehicle.class, new BcmsSessionPoliceVehiclePK(session.getSessionId(), police_vehicle));
        _police_vehicule.setPoliceVehicleStatus(status);
        _entity_manager.merge(_police_vehicule);
    }

    /**
     * On recupere dans la BD les donnees qui concerne les vehicules actifs dans la session en cours pour les afficher.
     *
     * @return
     */
    public static java.util.List<BcmsSessionPoliceVehicle> get_all_police_vehicle_session(String session) {
        assert (_entity_manager != null);
        java.util.List<BcmsSessionPoliceVehicle> pv= _entity_manager.createNamedQuery("BcmsSessionPoliceVehicle.findBySessionId").setParameter("sessionId", session).getResultList();

        Collections.sort(pv,new pvSessionComparator());
        
        return pv;
    }

    /**
     * Insert a police vehicle if the user ask a number of vehicle greater than th database can afford
     *
     * @param deb
     * @param fin
     */
    public static void insert_police_vehicle(int deb, int fin) {
        try {
            PoliceVehicle police = new PoliceVehicle();
            for (int i = deb; i <= fin; i++) {
                police.setPoliceVehicleName("PV" + i);
                _entity_manager.persist(police);
                flushAndClear();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * Count the total number of fire truck inside the database and return this number
     *
     * @return
     */
    public static int count_police_vehicle() {
        assert (_entity_manager != null);
        return ((Long) _entity_manager.createNamedQuery("PoliceVehicle.countPoliceVehicle").getSingleResult()).intValue();
    }

    /**
     * Get all police vehicle from database
     *
     * @return
     */
    public static java.util.List<PoliceVehicle> get_all_police_vehicle() {
        assert (_entity_manager != null);
        java.util.List<PoliceVehicle> pv = _entity_manager.createNamedQuery("PoliceVehicle.findAll").getResultList();

        java.util.Collections.sort(pv, new pvComparator());

        return pv;
    }

    // ***** FIRE TRUCK ***** //
    /**
     * Insert fire truck session in the data base. insert the name of the truck, his status and the active session
     */
    public static void insert_session_fire_truck(BcmsSession session, FireTruck fire_truck, String status) {
        BcmsSessionFireTruck _fire_truck = new BcmsSessionFireTruck(session.getSessionId(), fire_truck.getFireTruckName());
        _fire_truck.setFireTruckStatus(status);
        _fire_truck.setBcmsSession(session);
        _fire_truck.setFireTruck(fire_truck);
        _entity_manager.persist(_fire_truck);
    }

    public static void update_session_fire_truck(BcmsSession session, String fire_truck, String status) {
        BcmsSessionFireTruck _fire_truck = _entity_manager.find(BcmsSessionFireTruck.class, new BcmsSessionFireTruckPK(session.getSessionId(), fire_truck));
        _fire_truck.setFireTruckStatus(status);
        _entity_manager.merge(_fire_truck);
    }

    /**
     * On recupere dans la BD les donnees qui concerne les vehicules actifs dans la session en cours pour les afficher.
     *
     * @return
     */
    public static java.util.List<BcmsSessionFireTruck> get_all_fire_truck_session(String session) {
        assert (_entity_manager != null);
        java.util.List<BcmsSessionFireTruck> ft = _entity_manager.createNamedQuery("BcmsSessionFireTruck.findBySessionId").setParameter("sessionId", session).getResultList();
  
        java.util.Collections.sort(ft, new ftSessionComparator());
        
        return ft;
    }

    /**
     * Insert a fire truck if the user ask a number of truck greater than th database can afford
     *
     * @param deb
     * @param fin
     */
    public static void insert_fire_truck(int deb, int fin) {
        try {
            FireTruck fireTruck = new FireTruck();
            for (int i = deb; i <= fin; i++) {
                fireTruck.setFireTruckName("FT" + i);
                _entity_manager.persist(fireTruck);
                flushAndClear();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * Count the total number of fire truck inside the database and return this number
     *
     * @return
     */
    public static int count_fire_truck() {
        assert (_entity_manager != null);
        return ((Long) _entity_manager.createNamedQuery("FireTruck.countFireTruck").getSingleResult()).intValue();
    }

    /**
     * Get all fire truck from database
     *
     * @return
     */
    public static java.util.List<FireTruck> get_all_fire_truck() {
        assert (_entity_manager != null);
        java.util.List<FireTruck> ft = _entity_manager.createNamedQuery("FireTruck.findAll").getResultList();

        java.util.Collections.sort(ft, new ftComparator());

        return ft;
    }

    private static class pvComparator implements Comparator<PoliceVehicle> {

        @Override
        public int compare(PoliceVehicle o1, PoliceVehicle o2) {
            int n1 = Integer.parseInt(o1.getPoliceVehicleName().substring(2));
            int n2 = Integer.parseInt(o2.getPoliceVehicleName().substring(2));
            return n1 - n2;
        }
    }

    private static class ftComparator implements Comparator<FireTruck> {

        @Override
        public int compare(FireTruck o1, FireTruck o2) {
            int n1 = Integer.parseInt(o1.getFireTruckName().substring(2));
            int n2 = Integer.parseInt(o2.getFireTruckName().substring(2));
            return n1 - n2;
        }
    }

    private static class ftSessionComparator implements Comparator<BcmsSessionFireTruck> {

        @Override
        public int compare(BcmsSessionFireTruck o1, BcmsSessionFireTruck o2) {
            return (new ftComparator()).compare(o1.getFireTruck(),o2.getFireTruck());
        }
    }

    private static class pvSessionComparator implements Comparator<BcmsSessionPoliceVehicle> {

        @Override
        public int compare(BcmsSessionPoliceVehicle o1, BcmsSessionPoliceVehicle o2) {
            return (new pvComparator()).compare(o1.getPoliceVehicle(),o2.getPoliceVehicle());
        }
    }

}
