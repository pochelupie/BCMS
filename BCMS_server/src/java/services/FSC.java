package services;

import com.pauware.pauware_engine._Exception.Statechart_exception;
import entity_bean.BcmsSession;
import entity_bean.BcmsSessionFireTruck;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface FSC {
    //connection :
    public void FSC_connection_request() throws Statechart_exception;
    public BcmsSession FSC_BCMSsession();//don't used
    
    //route management :
    public void FSC_agrees_about_fire_truck_route() throws Statechart_exception; //firemen only
    public void FSC_disagrees_about_fire_truck_route() throws Statechart_exception;//firmen only
    public void FSC_agrees_about_police_vehicle_route() throws Statechart_exception ;//firemen only
    public void FSC_disagrees_about_police_vehicle_route() throws Statechart_exception;//firemen only
    
    
    //vehicle management :
    public void fire_truck_dispatched(String fire_truck) throws Statechart_exception;
    public void fire_truck_arrived(String fire_truck) throws Statechart_exception;
    public void fire_truck_breakdown(String fire_truck,/* may be 'null' */ String replacement_fire_truck) throws Statechart_exception;
    public void fire_truck_blocked(String fire_truck) throws Statechart_exception;
    public void enough_fire_trucks_arrived() throws Statechart_exception;
    public void enough_fire_trucks_dispatched() throws Statechart_exception ;
    public List<BcmsSessionFireTruck> get_list_fire_truck_by_session();
    
    //event management :
    public String getEventFSC();   
    
    //instanciation des vehicules :
    public void state_fire_truck_number(int number_of_fire_truck_required) throws Statechart_exception;
}
