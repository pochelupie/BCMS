package services;

import com.pauware.pauware_engine._Exception.Statechart_exception;
import entity_bean.BcmsSession;
import entity_bean.BcmsSessionPoliceVehicle;
import entity_bean.PoliceVehicle;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface PSC {
    //connection :
    public void PSC_connection_request() throws Statechart_exception ;
    public BcmsSession PSC_BCMSsession();//don't used
     
    //route management :
    public String getAllRoute() throws Statechart_exception;
    public void no_more_route_left() throws Statechart_exception;
    public void route_for_police_vehicles(String route_name) throws Statechart_exception;
    public void route_for_fire_trucks(String route_name) throws Statechart_exception;
    
    //vehicle management :
    public void police_vehicle_dispatched(String police_vehicle) throws Statechart_exception;
    public void police_vehicle_arrived(String police_vehicle) throws Statechart_exception;
    public void police_vehicle_breakdown(String police_vehicle,/* may be 'null' */ String replacement_police_vehicle) throws Statechart_exception;
    public void police_vehicle_blocked(String police_vehicle) throws Statechart_exception;
    public void enough_police_vehicles_arrived() throws Statechart_exception;
    public void enough_police_vehicles_dispatched() throws Statechart_exception ;
    public List<BcmsSessionPoliceVehicle> get_list_police_vehicle_by_session();
    
    //instanciation des vehicules :
    public void state_police_vehicle_number(int number_of_police_vehicle_required) throws Statechart_exception;
    
    //event management :
    public String getEventPSC();
    
    //crisis management :
    public void crisis_is_more_severe() throws Statechart_exception ;
    public void crisis_is_less_severe() throws Statechart_exception ; 
}
