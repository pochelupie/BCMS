package com.pauware.pauware_view;

final class Run_to_completion_data {

    final String _verbose;
    final java.util.Hashtable _execution;

    Run_to_completion_data(final String verbose, final java.util.Hashtable execution) {
        _verbose = verbose;
        _execution = execution;
    }
}

public final class PauWare_view extends Thread implements com.pauware.pauware_engine._Core.AbstractStatechart_monitor_listener, org.jwebsocket.listener.WebSocketServerTokenListener {

    public PauWare_view() {
        super("PauWare view"); // Thread instance's name
    }
    /**
     * JWebSocket
     */
    private static org.jwebsocket.server.TokenServer _TokenServer = null;
    private volatile org.jwebsocket.api.WebSocketConnector _my_view = null;
    private volatile boolean _stop = false;

    static {
// Mandatory line from http://jwebsocket.org/jwsForum219/posts/list/15619.page
        org.jwebsocket.factory.JWebSocketFactory.printCopyrightToConsole();
// Check if home, config or bootstrap path are passed by command line (mandatory as well):  
        org.jwebsocket.config.JWebSocketConfig.initForConsoleApp(null);
        org.jwebsocket.factory.JWebSocketFactory.start();
        _TokenServer = org.jwebsocket.factory.JWebSocketFactory.getTokenServer();
//        Runnable runnable = new Runnable() {
//            public void run() {
//                org.jwebsocket.factory.JWebSocketFactory.stop();
//            }
//        };
//        Runtime.getRuntime().addShutdownHook(new Thread(runnable));
    }

    /**
     * PauWare
     */
    private volatile long _animation_frequency = 3000L; // Adjust the slider range in 'index.html' accordingly
    private final java.util.concurrent.locks.ReadWriteLock _rwl = new java.util.concurrent.locks.ReentrantReadWriteLock();
    private final java.util.LinkedList<Run_to_completion_data> _run_to_completion_data = new java.util.LinkedList();
    private com.pauware.pauware_engine._Core.AbstractStatechart_monitor _state_machine = null;
    private String _state_machine_name = null;
    /**
     * JSON
     */
    public final static String Allowed_events = "allowed_events";
    public final static String Exits = "exits";
    public final static String Disactivated_states = "disactivated_states";
    public final static String Activated_transitions = "activated_transitions";
    public final static String Entries = "entries";
    public final static String Dos = "dos";
    public final static String Activated_states = "activated_states";
    public final static String True_invariants = "true_invariants";
    public final static String Untrue_invariants = "untrue_invariants";
    /**
     * PlantUML
     */
    public final static String PlantUML_start = "@startuml\n";
    public final static String PlantUML_end = "\n@enduml";
    /**
     * Web
     */
    public final static String PauWare_view_HTML_section = "<!--" + PauWare_view.class.getSimpleName() + "-->";
    private java.nio.file.Path _index_html_file = null;

    @Override
    public void post_construct(final com.pauware.pauware_engine._Core.AbstractStatechart_monitor state_machine) throws Exception {
        _state_machine = state_machine;
        _state_machine_name = com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(_state_machine.name());
        try {
            if (_TokenServer != null) {
                _TokenServer.addListener(this);
                // 'createDirectories' does not raise a 'FileAlreadyExistsException' when directory already exists:
                java.nio.file.Files.createDirectories(java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName())).resolve(_state_machine_name));
                _index_html_file = java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName()) + java.io.File.separator + _state_machine_name + java.io.File.separator + "index.html");
                java.nio.file.Files.copy((new java.io.File(System.getenv(PauWare_view.class.getSimpleName()) + java.io.File.separator + "index.html")).toPath(), _index_html_file, java.nio.file.StandardCopyOption.REPLACE_EXISTING);
                browse();
                // This starts the thread in charge of sending the run-to-completion data:
                start();
            } else {
                java.util.logging.Logger.getLogger(PauWare_view.class.getSimpleName()).log(java.util.logging.Level.SEVERE, "jWebSocket software (server side) is known as being located here: " + System.getenv(org.jwebsocket.config.JWebSocketServerConstants.JWEBSOCKET_HOME));
                java.util.logging.Logger.getLogger(PauWare_view.class.getSimpleName()).log(java.util.logging.Level.SEVERE, "However, the default token server declared in " + org.jwebsocket.config.JWebSocketServerConstants.JWEBSOCKET_XML + " has not been started...");
            }
        } catch (java.io.IOException ioe) {
            java.util.logging.Logger.getLogger(PauWare_view.class.getSimpleName()).log(java.util.logging.Level.SEVERE, "Fatal error -post_construct- in " + _state_machine_name, ioe);
            java.nio.file.Files.deleteIfExists(_index_html_file);
            java.nio.file.Files.deleteIfExists(java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName())).resolve(_state_machine_name));
        } catch (Throwable t) {
            java.util.logging.Logger.getLogger(PauWare_view.class.getSimpleName()).log(java.util.logging.Level.SEVERE, "Fatal error -post_construct- in " + _state_machine_name, t);
            if (_TokenServer != null) {
                _TokenServer.removeListener(this);
            }
        }
    }

    @Override
    public void start(final String verbose) throws Exception {
        java.util.concurrent.locks.Lock l = _rwl.writeLock();
        try {
            l.lock();
            _run_to_completion_data.addLast(new Run_to_completion_data(verbose, null));
        } finally {
            l.unlock();
        }
    }

    @Override
    public void run_to_completion(final String verbose, final java.util.Hashtable execution) throws Exception {
        java.util.concurrent.locks.Lock l = _rwl.writeLock();
        try {
            l.lock();
            _run_to_completion_data.addLast(new Run_to_completion_data(verbose, (java.util.Hashtable) execution.clone()));
        } finally {
            l.unlock();
        }
    }

    @Override
    public void stop(final String verbose) throws Exception {
        java.util.concurrent.locks.Lock l = _rwl.writeLock();
        try {
            l.lock();
            _run_to_completion_data.addLast(new Run_to_completion_data(verbose, null));
        } finally {
            l.unlock();
        }
    }

    @Override
    synchronized public void pre_destroy() throws Exception {
        try {
            if (!_stop) { // Maybe the view has already been destroyed in the Web browser...
                wait(); // Sending the remaining run-to-completion data...
                // This stops the thread in charge of sending the run-to-completion data:
                _stop = true;
                if (_TokenServer != null) {
                    _TokenServer.removeListener(this);
                }
            }
            java.nio.file.Files.deleteIfExists(java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName()) + java.io.File.separator + _state_machine_name + java.io.File.separator + _state_machine_name + ".svg"));
            java.nio.file.Files.deleteIfExists(_index_html_file);
            java.nio.file.Files.deleteIfExists(java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName())).resolve(_state_machine_name));
        } catch (Throwable t) {
            java.util.logging.Logger.getLogger(PauWare_view.class.getSimpleName()).log(java.util.logging.Level.SEVERE, "Fatal error -pre_destroy- in " + _state_machine_name, t);
        }
    }

    private void browse() throws java.io.IOException {
        assert (_index_html_file != null);
// Compute SVG:        
        net.sourceforge.plantuml.SourceStringReader reader = new net.sourceforge.plantuml.SourceStringReader(PlantUML_start + _state_machine.to_PlantUML() + PlantUML_end);
        java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
        reader.generateImage(baos, new net.sourceforge.plantuml.FileFormatOption(net.sourceforge.plantuml.FileFormat.SVG));
        baos.close();
// Add 'id' for overall 'svg' section:
        String svg_section = baos.toString();
        svg_section = svg_section.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
        svg_section = svg_section.replace("<svg", "<svg id=\"" + PauWare_view.class.getSimpleName() + "\"");
// Add 'id' for all transition 'text' sections:
        String[] svg_sections = svg_section.split("\\" + com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator);
        svg_section = "";
        for (int i = 0; i < svg_sections.length; i += 3) {
            if (i != svg_sections.length - 1) {
                int position_of_last_less_than = svg_sections[i].lastIndexOf("<text");
                if (position_of_last_less_than != -1) {
                    /**
                     * UML guards within square brackets are removed from
                     * transition text 'id'
                     */
                    int position_of_last_left_square_bracket = svg_sections[i + 1].indexOf('[');
                    int position_of_last_right_square_bracket = svg_sections[i + 1].indexOf(']');
                    String transition_text_id = null;
                    if (position_of_last_left_square_bracket != -1 && position_of_last_right_square_bracket != -1) {
                        transition_text_id = svg_sections[i + 1].substring(0, position_of_last_left_square_bracket) + svg_sections[i + 1].substring(position_of_last_right_square_bracket + 1, svg_sections[i + 1].length());
                    } else {
                        transition_text_id = svg_sections[i + 1];
                    }
                    /**
                     * End of UML guards
                     */
                    /**
                     * The 'completion' label of UML completion events is made
                     * hidden. Caution: this code also makes invisible any
                     * '[guard]/action(s)' or any '/action(s)' associated with
                     * the completion event.
                     */

                    if (svg_sections[i + 1].equals(com.pauware.pauware_engine._Core.AbstractStatechart.Completion) || svg_sections[i + 1].equals(com.pauware.pauware_engine._Core.AbstractStatechart.Completion + '[') || svg_sections[i + 1].equals(com.pauware.pauware_engine._Core.AbstractStatechart.Completion + '/')) {
//                        svg_sections[i] = svg_sections[i].replaceFirst("<text ", "<text visibility=\"hidden\" ");
                        svg_sections[i + 1] = svg_sections[i + 1].replace(com.pauware.pauware_engine._Core.AbstractStatechart.Completion, "");
                    }
                    /**
                     * End of UML completion events
                     */
                    /**
                     * HCERES
                     */
                    //                    if (svg_sections[i + 1].equals("no_more_route_left")) {
                    //                        String mouseover = "<set attributeName=\"text-decoration\" to=\"line-through\" begin=\"" + com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(transition_text_id + svg_sections[i + 2]) + ".mouseover\" end=\"" + com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(transition_text_id + svg_sections[i + 2]) + ".mouseout\"/>";
                    //                        String mousedown = "<set attributeName=\"text-decoration\" to=\"line-through\" begin=\"" + com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(transition_text_id + svg_sections[i + 2]) + ".mousedown\"/>";
                    //                        svg_section += svg_sections[i].substring(0, position_of_last_less_than) + "<text id=\"" + com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(transition_text_id + svg_sections[i + 2]) + "\"" + " onclick=\"unfires('" + svg_sections[i + 1] + "'," + svg_sections[i + 2] + ");\" " + " text-decoration=\"underline\" " + svg_sections[i].substring(position_of_last_less_than + String.valueOf("<text").length()) + mouseover + mousedown + svg_sections[i + 1];
                    //                    }
                    /**
                     * End of HCERES
                     */
                    svg_section += svg_sections[i].substring(0, position_of_last_less_than) + "<text id=\"" + com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(transition_text_id + svg_sections[i + 2]) + "\"" + svg_sections[i].substring(position_of_last_less_than + String.valueOf("<text").length()) + svg_sections[i + 1];
                }
            } else {
                svg_section += svg_sections[i];
            }
        }
        /**
         * The 'entry/ ^Statechart_monitor.completion' and 'entry/
         * ^Java_MEStatechart_monitor.completion' labels of UML output states
         * are removed.
         */
        svg_section = svg_section.replaceAll(com.pauware.pauware_engine._Core.AbstractStatechart.Entry_header_text + "\\" + com.pauware.pauware_engine._Core.AbstractAction.SendSignalAction_symbol + com.pauware.pauware_engine._Java_EE.Statechart_monitor.class.getSimpleName() + '.' + com.pauware.pauware_engine._Core.AbstractStatechart.Completion, "");
        svg_section = svg_section.replaceAll(com.pauware.pauware_engine._Core.AbstractStatechart.Entry_header_text + "\\" + com.pauware.pauware_engine._Core.AbstractAction.SendSignalAction_symbol + com.pauware.pauware_engine._Java_ME.Java_MEStatechart_monitor.class.getSimpleName() + '.' + com.pauware.pauware_engine._Core.AbstractStatechart.Completion, "");

        /**
         * End of UML output states
         */
// For testing purposes only (SVG file is created for late printing):
//        java.nio.file.Files.write(java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName()) + java.io.File.separator + _state_machine_name + java.io.File.separator + _state_machine_name + ".svg"), svg_section.getBytes());
// Insert SVG source in HTML file:
        java.util.List<String> lines = java.nio.file.Files.readAllLines(_index_html_file, java.nio.charset.Charset.defaultCharset());
        for (String line : lines) {
            if (line.contains("<title>PauWare view software</title>")) {
                lines.set(lines.indexOf(line), "<title>" + _state_machine_name + "</title>");
            }
            if (line.contains(PauWare_view_HTML_section)) {
                lines.set(lines.indexOf(line), svg_section);
                break;
            }
        }
// Overwrite HTML file:
        java.nio.file.Files.write(_index_html_file, lines, java.nio.charset.Charset.defaultCharset());
// Launch browser:
        java.awt.Desktop.getDesktop().browse(_index_html_file.toUri());
    }

    @Override
    public void processToken(org.jwebsocket.listener.WebSocketServerTokenEvent event, org.jwebsocket.token.Token token) {
        String state_machine_name = token.getString("state_machine_name");
        if (state_machine_name != null && state_machine_name.equals(_state_machine_name)) {
            if (_my_view == null) {
                _my_view = event.getConnector();
            }
            Double animation_frequency = token.getDouble("animation_frequency_changed");
            if (animation_frequency != null) {
                _animation_frequency = Math.round(animation_frequency * 1000.);
            }
        }
        /**
         * HCERES
         */
//        java.util.List<java.util.Map> l = token.getList("unfires");
//        if (l != null) {
//            for (java.util.Map m : l) {
//                for (java.util.Enumeration elements = _state_machine.transitions().keys(); elements.hasMoreElements();) {
//                    com.pauware.pauware_engine._Core.Transition transition = (com.pauware.pauware_engine._Core.Transition) elements.nextElement();
//                    int transition_hashCode = (Integer) m.get("transition_id");
//                    if (transition_hashCode == transition.hashCode()) {
//                        try {
//                            String strong_problem_here = ((String) m.get("event")).replace(com.pauware.pauware_engine._Core.AbstractStatechart.Neutral_character, ' ');
//                            _state_machine.unfires(strong_problem_here, transition.from(), transition.to(), true, null, null, null, com.pauware.pauware_engine._Core.AbstractStatechart.No_reentrance);
//                            java.nio.file.Files.createDirectories(java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName())).resolve(_state_machine_name + "unfires"));
//                            _index_html_file = java.nio.file.FileSystems.getDefault().getPath(System.getenv(PauWare_view.class.getSimpleName()) + java.io.File.separator + _state_machine_name + "unfires" + java.io.File.separator + "index.html");
//                            java.nio.file.Files.copy((new java.io.File(System.getenv(PauWare_view.class.getSimpleName()) + java.io.File.separator + "index.html")).toPath(), _index_html_file, java.nio.file.StandardCopyOption.REPLACE_EXISTING);
//                            browse();
//                        } catch (Exception e) {
//                            java.util.logging.Logger.getLogger(PauWare_view.class.getSimpleName()).log(java.util.logging.Level.SEVERE, null, e);
//                        }
//                    }
//                }
//            }
//        }
        /**
         * End of HCERES
         */
    }

    @Override
    public void processClosed(org.jwebsocket.kit.WebSocketServerEvent event
    ) {
        if (_my_view != null && _my_view == event.getConnector()) {
            // This stops the thread in charge of sending the run-to-completion data:
            _stop = true;
            if (_TokenServer != null) {
                _TokenServer.removeListener(this);
            }
        }
    }

    @Override
    public void processOpened(org.jwebsocket.kit.WebSocketServerEvent event
    ) {
    }

    @Override
    public void processPacket(org.jwebsocket.kit.WebSocketServerEvent event, org.jwebsocket.api.WebSocketPacket packet
    ) {
    }

    @Override
    public void run() {
        while (!_stop) {
            try {
                Thread.sleep(_animation_frequency);
            } catch (InterruptedException ie) {
                java.util.logging.Logger.getLogger(PauWare_view.class.getName()).log(java.util.logging.Level.SEVERE, null, ie);
            }
            if (_my_view != null) {
                send();
            }
        }
        if (_my_view != null) {
            _my_view.stopConnector(org.jwebsocket.kit.CloseReason.SHUTDOWN);
        }
        if (_TokenServer.getAllConnectors().isEmpty()) {
            org.jwebsocket.factory.JWebSocketFactory.stop();
        }
    }

    synchronized private void send() {
        Run_to_completion_data rtcd = null;
        java.util.concurrent.locks.Lock l = _rwl.readLock();
        try {
            l.lock();
            rtcd = _run_to_completion_data.removeFirst();
        } catch (java.util.NoSuchElementException nsee) {
            notify();
        } finally {
            l.unlock();
        }
        if (rtcd != null) {
            javax.json.JsonArrayBuilder allowed_events = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder exits = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder disactivated_states = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder activated_transitions = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder entries = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder dos = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder activated_states = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder true_invariants = javax.json.Json.createArrayBuilder();
            javax.json.JsonArrayBuilder untrue_invariants = javax.json.Json.createArrayBuilder();
            String verboses[] = rtcd._verbose.split("\n");
            for (int i = 0; i < verboses.length; i++) {
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Allowed_event_display_message)) {
                    String allowed_event = verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Allowed_event_display_message) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Allowed_event_display_message).length());
                    allowed_events.add(com.pauware.pauware_engine._Core.AbstractStatechart.Allow_header_text + allowed_event.substring(0, allowed_event.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator)));
                }
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Exit_action_display_message)) {
                    String exit = verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Exit_action_display_message) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Exit_action_display_message).length());
                    exits.add(com.pauware.pauware_engine._Core.AbstractStatechart.Exit_header_text + exit.substring(0, exit.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator)));
                }
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Disactivated_state_display_message)) {
                    disactivated_states.add(com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Disactivated_state_display_message) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Disactivated_state_display_message).length())));
                }
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Entry_action_display_message)) {
                    String entry = verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Entry_action_display_message) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Entry_action_display_message).length());
                    entries.add(com.pauware.pauware_engine._Core.AbstractStatechart.Entry_header_text + entry.substring(0, entry.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator)));
                }
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Do_activity_display_message)) {
                    String a_do = verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Do_activity_display_message) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Do_activity_display_message).length());
                    dos.add(com.pauware.pauware_engine._Core.AbstractStatechart.Do_header_text + a_do.substring(0, a_do.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator)));
                }
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Activated_state_display_message)) {
                    activated_states.add(com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Activated_state_display_message) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Activated_state_display_message).length())));
                }
                if (verboses[i].contains(com.pauware.pauware_engine._Core.AbstractStatechart.Invariant_header_text)) {
                    String invariant = verboses[i].substring(verboses[i].indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Invariant_header_text) + String.valueOf(com.pauware.pauware_engine._Core.AbstractStatechart.Invariant_header_text).length());
                    if (invariant.substring(invariant.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator) + 1).equals("true")) {
                        true_invariants.add(com.pauware.pauware_engine._Core.AbstractStatechart.Invariant_header_text + invariant.substring(0, invariant.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator)));
                    } else {
                        untrue_invariants.add(com.pauware.pauware_engine._Core.AbstractStatechart.Invariant_header_text + invariant.substring(0, invariant.indexOf(com.pauware.pauware_engine._Core.AbstractStatechart.Textual_view_subject_separator)));
                    }
                }
            }
            if (rtcd._execution != null) {
                for (java.util.Enumeration elements = rtcd._execution.keys(); elements.hasMoreElements();) {
                    com.pauware.pauware_engine._Core.Transition transition = (com.pauware.pauware_engine._Core.Transition) elements.nextElement();
                    Object[] actions = (Object[]) rtcd._execution.get(transition);
                    String transition_text = verboses[0].substring(verboses[0].lastIndexOf('.') + 1);
                    for (int i = 0; i < actions.length; i++) {
                        transition_text += com.pauware.pauware_engine._Core.AbstractStatechart.Neutral_character + ((com.pauware.pauware_engine._Core.AbstractAction) actions[i]).to_UML();
                    }
                    activated_transitions.add(com.pauware.pauware_engine._Core.AbstractStatechart.Clean_up(transition_text + transition.hashCode()));
                }
            }
            javax.json.JsonObjectBuilder builder = javax.json.Json.createObjectBuilder();
            builder.add("state_machine_name", _state_machine_name);
            builder.add("verbose", rtcd._verbose);
            builder.add(Allowed_events, allowed_events);
            builder.add(Exits, exits);
            builder.add(Disactivated_states, disactivated_states);
            builder.add(Activated_transitions, activated_transitions);
            builder.add(Entries, entries);
            builder.add(Activated_states, activated_states);
            builder.add(Dos, dos);
            builder.add(True_invariants, true_invariants);
            builder.add(Untrue_invariants, untrue_invariants);
            assert (_TokenServer != null && _TokenServer.isAlive() && _my_view != null);
            _TokenServer.sendToken(_my_view, org.jwebsocket.packetProcessors.JSONProcessor.JSONStringToToken(builder.build().toString()));
        }
    }
}
